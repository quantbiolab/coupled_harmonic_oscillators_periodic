# Coupled_Harmonic_Oscillators_Periodic
A python instance to simulate a periodic arrangement of coupled harmonic oscillators and to analyze
their principal components. Coded for the publication titled
"Spurious negative eigenvalues of numerical variance-covariance matrices in
many-body systems correlate with the existence of frozen degrees of freedom"
which has been published here: https://iopscience.iop.org/article/10.1088/1402-4896/ad623c .

Use MyOscillators/Oscillators.py and MyOscillators/animation.py if you only want to simulate and animate
coupled oscillators. TestSimulation.py may be a good start if that is your only goal.
